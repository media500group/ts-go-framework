package redis_feed

import (
	"github.com/go-redis/redis"
	"reflect"
	"encoding/json"
	"errors"
	"fmt"
)

type caller struct {
	Func        reflect.Value
	Args        reflect.Type
	ArgsPresent bool
	Out         bool
}

type RedisFeed struct {
	RedisClient *redis.Client
}

func NewRedisFeed(options *redis.Options) (*RedisFeed) {
	service := new(RedisFeed)
	service.RedisClient = redis.NewClient(options)
	return service
}

func (s RedisFeed) SubscribeChannel(channel string, f interface{}) (error) {
	c, err := newCaller(f)
	if err != nil {
		return errors.New("redisSubscribe: Error while creating caller")
	}
	data := reflect.New(c.Args).Interface()

	//Check connect
	cmd := s.RedisClient.Ping()
	_, err = cmd.Result()
	if err != nil {
		fmt.Println(fmt.Errorf("Error while send ping: %v", err))
  		return err
	}

	go func() (error) {
		pubSub := s.RedisClient.Subscribe(channel)
		defer pubSub.Close()

		for {
			err := pubSub.Ping("")
			if err != nil {
				fmt.Println(fmt.Errorf("redisSubscribe: Error while send ping: %v", err))
				continue
			}

			msg, err := pubSub.ReceiveMessage()
			if err != nil {
				fmt.Println(fmt.Errorf("redisSubscribe: Error while recieving message: %v", err))
				continue
			}

			err = json.Unmarshal([]byte(msg.Payload), &data)
			if err != nil {
				fmt.Println(fmt.Errorf("redisSubscribe: Error while unmarshall: %v", err))
				continue
			}

			a := []reflect.Value{reflect.ValueOf(data).Elem()}
			c.Func.Call(a)
		}
	} ()

	return nil
}


func newCaller(f interface{}) (*caller, error) {
	fVal := reflect.ValueOf(f)
	if fVal.Kind() != reflect.Func {
		return nil, errors.New("newCaller: Error")
	}

	fType := fVal.Type()
	if fType.NumOut() > 1 {
		return nil, errors.New("newCaller: Error")
	}

	curCaller := &caller{
		Func: fVal,
		Out:  fType.NumOut() == 1,
	}

	if fType.NumIn() == 1 {
		curCaller.Args = fType.In(0)
		curCaller.ArgsPresent = true
	} else {
		return nil, errors.New("newCaller: Error")
	}

	return curCaller, nil
}