package middleware

import (
	"sync/atomic"
	"time"
	_ "expvar"
	"github.com/labstack/echo"
	"net/http"
	log "github.com/sirupsen/logrus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/client_golang/prometheus"
	"sync"
)

type RequestStats struct{
	requestCount uint32
	responceTime uint64
	AvgResponseTime	uint32
	RPS				uint32
}

type (
	ServerPrometheusStats struct {
		requestQuery uint32
		ticker       *time.Ticker
		requestsStats *sync.Map
		RequestQuery	uint32
	}
)

func (s *ServerPrometheusStats) ProcessRequestStats(path string, time uint64) {
	v, ok := s.requestsStats.Load(path)
	var requestStats *RequestStats

	if !ok {
		requestStats = &RequestStats{}
		s.requestsStats.Store(path, requestStats)
	} else {
		requestStats = v.(*RequestStats)
	}

	atomic.AddUint32(&requestStats.requestCount, 1)
	atomic.AddUint64(&requestStats.responceTime, time)
}

func (s *ServerPrometheusStats) Middleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		responseStart := time.Now()
		atomic.AddUint32(&s.requestQuery, 1)
		if err := next(c); err != nil {
			c.Error(err)
		}
		atomic.AddUint32(&s.requestQuery, ^uint32(0))
		responseTime := uint64(time.Now().Sub(responseStart) / time.Millisecond)
		s.ProcessRequestStats(c.Path(), responseTime)
		return nil
	}
}
func NewPrometheusMetrics(e *echo.Echo, debugVarHost string, debugVarExpose bool) *ServerPrometheusStats {
	stats := &ServerPrometheusStats{
		ticker: time.NewTicker(time.Second),
		requestsStats: new(sync.Map),
	}

	if(debugVarHost != "") {
		log.WithField("host", debugVarHost).Info("Expose private debug vars")
		go func() { http.ListenAndServe(debugVarHost, promhttp.Handler()) }()
	}

	go func() {
		var (
			rpsProm = prometheus.NewGaugeVec(
				prometheus.GaugeOpts{
					Name:       "request_per_second",
					Help:       "Count of rsequest per second.",
				},
				[]string{"service"},
			)

			requestQueryProm = prometheus.NewGauge(
				prometheus.GaugeOpts{
					Name:       "request_query",
					Help:       "Request query.",
				},
			)

			avgResponseTimeProm = prometheus.NewGaugeVec(
				prometheus.GaugeOpts{
					Name:       "avg_response_time",
					Help:       "Average response time distributions.",
				},
				[]string{"service"},
			)

			uptimeProm = prometheus.NewCounter(
				prometheus.CounterOpts{
					Name: "start_time",
					Help: "Start time of service.",
				},
			)

		)

		prometheus.MustRegister(rpsProm)
		prometheus.MustRegister(requestQueryProm)
		prometheus.MustRegister(avgResponseTimeProm)
		prometheus.MustRegister(uptimeProm)

		var timestamp time.Time
		startTime := time.Now()
		lastTick := startTime

		uptimeProm.Add(float64(startTime.Unix()))

		ExposeStats := func(key, value interface{}) bool {
			label := key.(string)
			stats := value.(*RequestStats)

			requestCount := atomic.LoadUint32(&stats.requestCount)
			responseTime := atomic.LoadUint64(&stats.responceTime)
			rps := float64(requestCount) / timestamp.Sub(lastTick).Seconds()

			avgResponseTimeStat := atomic.LoadUint32(&stats.AvgResponseTime)

			rpsStat := uint32(rps)
			if requestCount != 0 {
			avgResponseTimeStat = uint32(float64(responseTime)/float64(requestCount))
			}

			atomic.StoreUint32(&stats.RPS, rpsStat)
			atomic.StoreUint32(&stats.requestCount, 0)
			atomic.StoreUint64(&stats.responceTime, 0)
			atomic.StoreUint32(&stats.AvgResponseTime, avgResponseTimeStat)

			rpsProm.WithLabelValues(label).Set(rps)
			avgResponseTimeProm.WithLabelValues(label).Set(float64(avgResponseTimeStat) / timestamp.Sub(lastTick).Seconds() )

			return true
		}

		for timestamp = range stats.ticker.C {
			//REQUEST QUERY
			requestQueryStat := atomic.LoadUint32(&stats.requestQuery)
			atomic.StoreUint32(&stats.RequestQuery, requestQueryStat)
			requestQueryProm.Set(float64(requestQueryStat))

			//BY PATHES
			stats.requestsStats.Range(ExposeStats)
			lastTick = timestamp
		}
	}()

	e.GET( "/healthcheck", func(c echo.Context) error {
		s := *stats
		return c.JSON(200, s) })

	if debugVarExpose {
		log.Warn("Private debug vars exposed in public")
		e.GET("/debug/metrics", func(c echo.Context) error {
			w := c.Response()
			r := c.Request()
			h := promhttp.Handler()
			h.ServeHTTP(w, r)
			return  c.JSON(http.StatusOK, nil)
		})
	}

	return stats
}