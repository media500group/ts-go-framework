package middleware

import (
	"github.com/labstack/echo"
	"net/http"
	"bufio"
	"time"
	"strings"
	"io"
	"encoding/json"
	"bytes"
	log "github.com/sirupsen/logrus"
)

type ResponseMetaInfo struct{
	Time int	`json:"time"`
	Count int	`json:"count"`
}

type ResponseMetaResponse struct {
	Code int		`json:"code"`
	Message interface{}	`json:"message"`
}

type ResponseMeta struct{
	Info  ResponseMetaInfo 		  `json:"info"`
	Response ResponseMetaResponse `json:"response"`
}

type responseWriter struct {
	header http.Header
	writer *bufio.Writer
	status int
}

func (r *responseWriter) Header() (http.Header) {
	return r.header
}

func (r *responseWriter) Write(p []byte) (int, error){
	return r.writer.Write(p)
}

func (r *responseWriter) WriteHeader(h int) {
	r.status = h
}

func (r *responseWriter) Flush() (error){
	return r.writer.Flush()
}

func ResponseWrapper() echo.MiddlewareFunc {
	// Process is the middleware function.
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) (err error) {
			req := ctx.Request()
			res := ctx.Response()
			start := time.Now()
			var b bytes.Buffer

			oWriter := res.Writer
			rWriter := responseWriter{
				header: res.Header(),
				writer: bufio.NewWriter(&b),
			}
			res.Writer = http.ResponseWriter(&rWriter)

			var httpCode int
			var message interface{}
			if err := next(ctx); err != nil {
				log.WithError(err).WithField("Request:", req.RequestURI).Warnln("Request error")
				httpCode = http.StatusInternalServerError
				if he, ok := err.(*echo.HTTPError); ok {
					httpCode = he.Code
					message = he.Message
				}

				if !ctx.Response().Committed {
					ctx.JSON(httpCode, nil)
				}
			} else {
				httpCode = rWriter.status
				message = "OK"
			}

			meta := ResponseMeta{
				Info:     ResponseMetaInfo{Time: (int)(time.Since(start).Nanoseconds() / int64(time.Millisecond)), Count: 9},
				Response: ResponseMetaResponse{Code: httpCode, Message: message},
			}

			rWriter.Flush()
			res.Writer = oWriter
			res.Writer.WriteHeader(httpCode)

			if !strings.Contains(res.Header().Get(echo.HeaderContentType), echo.MIMEApplicationJSON) || req.RequestURI == "/debug/vars" || req.RequestURI == "/debug/metrics" {
				return ctx.Blob(httpCode, res.Header().Get(echo.HeaderContentType), b.Bytes())
			}

			io.WriteString(res.Writer, "{\"data\":")
			ctx.JSONBlob(httpCode, b.Bytes())
			io.WriteString(res.Writer, ",\"meta\":")
			if err := json.NewEncoder(res.Writer).Encode(meta); err != nil {
				return err
			}
			io.WriteString(res.Writer, "}")

			return nil
		}
	}
}