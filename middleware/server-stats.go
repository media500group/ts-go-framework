package middleware

import (
	"sync/atomic"
	"time"
	_ "expvar"
	"github.com/labstack/echo"
	"encoding/json"
	"net/http"
	"expvar"
	log "github.com/sirupsen/logrus"
)

type (
	ServerStats struct {
		uptime		 uint32
		requestCount uint32
		requestQuery uint32
		responceTime uint64
		AvgResponseTime	uint32
		RequestQuery	uint32
		RPS				uint32
		ticker       *time.Ticker
	}
)

func (s ServerStats) MarshalJSON() ([]byte, error) {
	basicLink := struct {
		Uptime 			string	`json:"uptime""`
		AvgResponseTime uint32	`json:"avg_response_time"`
		RequestQuery 	uint32	`json:"request_query"`
		RPS				uint32	`json:"rps"`
	}{
		Uptime: (time.Duration(s.uptime) * time.Second).String(),
		AvgResponseTime: s.AvgResponseTime,
		RequestQuery: s.RequestQuery,
		RPS: s.RPS,
	}

	return json.Marshal(basicLink)
}

func (s *ServerStats) Middleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		responseStart := time.Now()
		atomic.AddUint32(&s.requestQuery, 1)
		if err := next(c); err != nil {
			c.Error(err)
		}
		atomic.AddUint32(&s.requestQuery, ^uint32(0))
		responseTime := uint64(time.Now().Sub(responseStart) / time.Millisecond)
		atomic.AddUint32(&s.requestCount, 1)
		atomic.AddUint64(&s.responceTime, responseTime)
		return nil
	}
}

func NewServerStats(e *echo.Echo, debugVarHost string, debugVarExpose bool) *ServerStats {
	stats := &ServerStats{
		ticker: time.NewTicker(time.Second),
	}

	if(debugVarHost != "") {
		log.WithField("host", debugVarHost).Info("Expose private debug vars")
		go func() { http.ListenAndServe(debugVarHost, nil) }()
	}

	go func() {
		startTime := time.Now()
		lastTick := startTime

		var (
			uptimeExp = expvar.NewInt("uptime")
			rpsExp	   = expvar.NewInt("rps")
			requestQueryExp = expvar.NewInt("request_query")
			avgResponseTimeExp = expvar.NewInt("avg_response_time")
		)

		for t := range stats.ticker.C {
			requestCount := atomic.LoadUint32(&stats.requestCount)
			responseTime := atomic.LoadUint64(&stats.responceTime)
			rps := float64(requestCount) / t.Sub(lastTick).Seconds()

			avgResponseTimeStat := atomic.LoadUint32(&stats.AvgResponseTime)
			uptimeStat := uint32(time.Since(startTime).Seconds())
			rpsStat := uint32(rps)
			requestQueryStat := atomic.LoadUint32(&stats.requestQuery)
			if requestCount != 0 {
				avgResponseTimeStat = uint32(float64(responseTime)/float64(requestCount))
			}

			atomic.StoreUint32(&stats.uptime, uptimeStat)
			atomic.StoreUint32(&stats.RPS, rpsStat)
			atomic.StoreUint32(&stats.RequestQuery, requestQueryStat)
			atomic.StoreUint32(&stats.requestCount, 0)
			atomic.StoreUint64(&stats.responceTime, 0)
			atomic.StoreUint32(&stats.AvgResponseTime, avgResponseTimeStat)

			uptimeExp.Set(int64(uptimeStat))
			rpsExp.Set(int64(rpsStat))
			requestQueryExp.Set(int64(requestQueryStat))
			avgResponseTimeExp.Set(int64(avgResponseTimeStat))

			lastTick = t
		}
	}()

	e.GET( "/healthcheck", func(c echo.Context) error {
		s := *stats
		return c.JSON(200, s) })

	if (debugVarExpose) {
		log.Warn("Private debug vars exposed in public")
		e.GET("/debug/vars", func(c echo.Context) error {
			w := c.Response()
			r := c.Request()
			if h, p := http.DefaultServeMux.Handler(r); p != "" {
				h.ServeHTTP(w, r)
				return nil
			}
			return echo.NewHTTPError(http.StatusNotFound)
		})
	}

	return stats
}