package framework

import (
	"github.com/olebedev/config"
	"github.com/labstack/echo"
	"github.com/sarulabs/di"
	"encoding/json"
	"os"
	"time"
)

type Controller interface {
	Init(ctx di.Container)
	RegisterRoute(e *echo.Echo)
}

type Service interface {
	Init(ctx di.Container)
}

type Repository interface {
	Init(ctx di.Container)
}

func RegisterController(builder *di.Builder, name string, controller Controller) error {
	return builder.Add(di.Def{
		Name: name,
		Build: func(ctx di.Container) (interface{}, error) {
			e := ctx.Get("echo").(*echo.Echo)
			controller.Init(ctx)
			controller.RegisterRoute(e)
			return controller,nil
		},
		Tags: []di.Tag{{Name: "framework_scope", Args: map[string]string{"type": "controller"}}},
	})
}

func RegisterService(builder *di.Builder, name string, service Service) error {
	return builder.Add(di.Def{
		Name: name,
		Build: func(ctx di.Container) (interface{}, error) {
			service.Init(ctx)
			return service,nil
		},
		Tags: []di.Tag{{Name: "framework_scope", Args: map[string]string{"type": "service"}}},
	})
}

func RegisterRepository(builder *di.Builder, name string, repository Repository) error {
	return builder.Add(di.Def{
		Name: name,
		Build: func(ctx di.Container) (interface{}, error) {
			repository.Init(ctx)
			return repository,nil
		},
		Tags: []di.Tag{{Name: "framework_scope", Args: map[string]string{"type": "repository"}}},
	})
}

func replaceEnv(_map map[string] interface{})  {
	for key, value := range _map {
		if child, ok := value.(map[string] interface{}); ok {
			replaceEnv(child)
			continue
		}

		if str, ok := value.(string); ok && len(str) > 0 && str[0] == '$'{
			_map[key] = os.Getenv(str[1:])
		}
	}
}

func LoadConfig(configuration string) (*config.Config, error) {
	cfg, err := config.ParseYamlFile("properties.yaml")
	if err != nil {
		return cfg, err
	}

	if cfg, err = cfg.Get(configuration); err != nil {
		return cfg, err
	}

	cfg = cfg.Env() //Keys as env values

	var configMap map[string] interface{}
	if configMap, err = cfg.Map(""); err != nil {
		return cfg, err
	}

	replaceEnv(configMap) //Values ($ENV_SOMETHING) as env

	if _, err = json.Marshal(cfg); err != nil {
		return cfg, err
	}

	return cfg, err
}

func StartWebServer(ctx di.Container) (error)  {
    for _, definition := range ctx.Definitions(){
		for _, tag := range definition.Tags{
			if tag.Name != "framework_scope" {
				continue
			}

			if _type, ok := tag.Args["type"]; ok == true {
				if _type == "controller" {
					_, err := ctx.SafeGet(definition.Name);
					if err != nil {
						return err
					}
				}
			}
		}
	}
	time.Sleep(time.Millisecond * 100)
	e, err := ctx.SafeGet("echo")
	if err != nil {
		return err
	}

	return e.(*echo.Echo).Start(ctx.Get("appHost").(string))
}