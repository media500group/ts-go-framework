package database

type DatabaseConfiguration struct {
	Host 		string
	Port		string
	Name 	    string
	User        string
	Password    string
}