package liquidbase

import (
	"bitbucket.org/media500group/ts-go-framework/database"
	"os/exec"
	"fmt"
	"github.com/sirupsen/logrus"
	"path/filepath"
	"strings"
	"os"
)

type LiquibaseConfiguration struct {
	Driver          string
	Command         string
	ChangeLogFile   string
	ClassPath 		string
}

type Liquibase struct {
	dbConfiguration database.DatabaseConfiguration
	config LiquibaseConfiguration
}

func New(dbConfig database.DatabaseConfiguration, config LiquibaseConfiguration) (*Liquibase, error) {
	lb := Liquibase{
		dbConfiguration: dbConfig,
		config: config,
	}

	return &lb,nil
}

func (lb *Liquibase) Update() error {
	return lb.execAction("update")
}

func (lb *Liquibase) execAction(action string) error  {
	logrus.Info("Start update with Liquidbase")
	fmt.Println(filepath.Abs("./"))

	commands := strings.Split(lb.config.Command, " ")
	commands = append(commands,"--logLevel=info")
	commands = append(commands, fmt.Sprintf("--changeLogFile=%s", lb.config.ChangeLogFile))
	commands = append(commands, fmt.Sprintf("--url=jdbc:postgresql://%s:%s/%s", lb.dbConfiguration.Host, lb.dbConfiguration.Port, lb.dbConfiguration.Name))
	commands = append(commands, fmt.Sprintf("--username=%s", lb.dbConfiguration.User))
	commands = append(commands, fmt.Sprintf("--password=%s", lb.dbConfiguration.Password))
	commands = append(commands, fmt.Sprintf("--driver=%s", lb.config.Driver))
	commands = append(commands, fmt.Sprintf("--classpath=%s", lb.config.ClassPath))
	commands = append(commands, action)

	cmd := exec.Command(commands[0],commands[1:]...)
	logrus.Info("Liquidbase command:", cmd.Args)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()

	if err != nil {
		return err
	}

	return nil
}