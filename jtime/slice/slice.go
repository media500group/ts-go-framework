package slice

import (
	"time"
	"bitbucket.org/media500group/ts-go-framework/jtime/period"
)

type Slice int

const (
	SECOND Slice = iota
	MINUTE Slice = iota
	HOUR   Slice = iota
	DAY    Slice = iota
)

func (s Slice) String() string {
	switch s {
	case SECOND:
		return "SECOND"
	case MINUTE:
		return "MINUTE"
	case HOUR:
		return "HOUR"
	case DAY:
		return "DAY"
	}
	return ""
}

func (s Slice) TurnicateTime(_time time.Time) time.Time {
	switch s {
	case SECOND:
		return time.Date(_time.Year(), _time.Month(), _time.Day(), _time.Hour(), _time.Minute(), _time.Second(), 0, _time.Location())
	case MINUTE: //MINUTE
		return time.Date(_time.Year(), _time.Month(), _time.Day(), _time.Hour(), _time.Minute(), 0, 0, _time.Location())
	case HOUR:	//HOUR
		return time.Date(_time.Year(), _time.Month(), _time.Day(), _time.Hour(), 0, 0, 0, _time.Location())
	case DAY:	//DAY
		return time.Date(_time.Year(), _time.Month(), _time.Day(), 0, 0, 0, 0, _time.Location())
	}
	return _time
}

func (s Slice) Period() period.Period{
	switch s {
	case SECOND:
		return period.FromDuration(time.Second)
	case MINUTE:
		return period.FromDuration(time.Minute)
	case HOUR:
		return period.FromDuration(time.Hour)
	case DAY:
		return period.FromCalendar(0,0,1)
	}
	return period.Period{}
}

func (s Slice) Duration() time.Duration{
	switch s {
	case SECOND:
		return time.Second
	case MINUTE:
		return time.Minute
	case HOUR:
		return time.Hour
	case DAY:
		return time.Hour*24
	}
	return time.Duration(0)
}

