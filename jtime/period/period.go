package period

import (
	"time"
)

type Period struct {
	duration time.Duration
	days	  int
	months    int
	years	  int
}

func (p *Period) AddToTime(time time.Time) (time.Time) {
	time = time.Add(p.duration)
	time = time.AddDate(p.years, p.months, p.days)
	return time
}

func (p *Period) SubFromTime(time time.Time) (time.Time)  {
	time = time.Add(-1 * p.duration)
	time = time.AddDate(-1 * p.years, -1 * p.months, -1 * p.days)
	return time
}

func New(years int, months int, days int, duration time.Duration) (Period) {
	return Period{
		duration: duration,
		years: years,
		months: months,
		days: days,
	}
}

func FromCalendar(years int, months int, days int) (Period) {
	return Period{
		years: years,
		months: months,
		days: days,
	}
}

func FromDuration(duration time.Duration) (Period) {
	return Period{
		duration: duration,
	}
}